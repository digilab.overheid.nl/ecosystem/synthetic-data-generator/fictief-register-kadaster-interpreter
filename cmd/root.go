package cmd

import "github.com/spf13/cobra"

var RootCmd = &cobra.Command{
	Use:   "api",
	Short: "frv interpreter",
	Long:  "Fictief register voertuigen interpreter",
}

func Execute() error {
	return RootCmd.Execute()
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	RootCmd.AddCommand(interpreterCommand)
}
