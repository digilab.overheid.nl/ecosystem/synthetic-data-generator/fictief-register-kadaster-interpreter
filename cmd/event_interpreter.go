package cmd

import (
	"fmt"
	"log"

	"github.com/nats-io/nats.go"
	"github.com/spf13/cobra"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-interpreter/application"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-interpreter/process"
)

var interpreterOpts struct {
	Nats struct {
		Host string
		Port string
	}
	BaseURL string
}

//nolint:gochecknoinits // this is the recommended way to use cobra
func init() {
	interpreterCommand.Flags().StringVarP(&interpreterOpts.Nats.Host, "sdg-nats-service-host", "", "127.0.0.1", "Address of NATS")
	interpreterCommand.Flags().StringVarP(&interpreterOpts.Nats.Port, "sdg-nats-service-port", "", "4222", "Port of NATS")

	interpreterCommand.Flags().StringVarP(&interpreterOpts.BaseURL, "sdg-backend-url", "", "http://0.0.0.0:8080", "Base URL of the Fictieve Register")
}

var interpreterCommand = &cobra.Command{
	Use:   "interpreter",
	Short: "Run the event interpreter",
	Run: func(cmd *cobra.Command, args []string) {
		p := process.NewProcess()

		nc, err := nats.Connect(fmt.Sprintf("nats://%s:%s", interpreterOpts.Nats.Host, interpreterOpts.Nats.Port))
		if err != nil {
			log.Fatal(fmt.Errorf("nats connect failed: %w", err))
		}

		fmt.Println("Starting")

		js, err := nc.JetStream()
		if err != nil {
			log.Fatal(fmt.Errorf("Jet stream failed: %w", err))
		}

		app := application.NewApplication(interpreterOpts.BaseURL)

		s, err := js.Subscribe("events.frk.>", app.HandleEvents)
		if err != nil {
			log.Fatal(fmt.Errorf("subscribe failed: %w", err))
		}

		p.Wait()

		if err := s.Unsubscribe(); err != nil {
			log.Fatal(fmt.Errorf("unsubscribe failed: %w", err))
		}

		if err := s.Drain(); err != nil {
			log.Fatal(fmt.Errorf("subscription drain failed: %w", err))
		}

		if err := nc.Drain(); err != nil {
			log.Fatal(fmt.Errorf("nats drain failed: %w", err))
		}

		nc.Close()
	},
}
