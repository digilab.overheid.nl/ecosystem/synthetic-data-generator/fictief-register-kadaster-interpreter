package adapter

import "gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-interpreter/model"

func ToPlot(event *model.Event, data *model.PlotRegisteredEvent) *model.PlotRegistered {
	return &model.PlotRegistered{
		ID:           event.SubjectIDs[0],
		Municipality: data.Municipality,
		Section:      data.Section,
		Number:       data.Number,
		Surface:      data.Surface,
	}
}

func ToOwnerDetermined(event *model.Event, data *model.PlotOwnersDeterminedEvent) *model.PlotOwnersDetermined {
	return &model.PlotOwnersDetermined{
		Owners: data.Owners,
	}
}

func ToReparcelled(event *model.Event, data *model.ReparcelledEvent) *model.Reparcelled {
	return &model.Reparcelled{
		From: event.SubjectIDs,
	}
}
