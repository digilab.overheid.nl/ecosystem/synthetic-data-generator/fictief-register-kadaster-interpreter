package model

import "github.com/google/uuid"

type Municipality struct {
	Code string `json:"code"`
	Name string `json:"name"`
}

type PlotRegisteredEvent struct {
	Municipality Municipality `json:"municipality"`
	Section      string       `json:"section"`
	Number       int32        `json:"number"`
	Surface      int32        `json:"surface"`
}

type PlotRegistered struct {
	ID           uuid.UUID    `json:"id"`
	Municipality Municipality `json:"municipality"`
	Section      string       `json:"section"`
	Number       int32        `json:"number"`
	Surface      int32        `json:"surface"`
}

type PlotOwnersDeterminedEvent struct {
	Owners []uuid.UUID `json:"owners"`
}

type PlotOwnersDetermined struct {
	Owners []uuid.UUID `json:"owners"`
}

type ReparcelledEvent struct {
}

type Reparcelled struct {
	From []uuid.UUID      `json:"from"`
	To   []PlotRegistered `json:"to"`
}
