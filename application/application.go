package application

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httputil"

	"github.com/nats-io/nats.go"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-interpreter/model"
)

type Application struct {
	client  *http.Client
	baseURL string
}

func NewApplication(baseURL string) *Application {
	return &Application{
		client:  http.DefaultClient,
		baseURL: baseURL,
	}
}

func (app *Application) HandleEvents(msg *nats.Msg) {
	event := &model.Event{}
	if err := json.Unmarshal(msg.Data, event); err != nil {
		fmt.Print(fmt.Errorf("unmarshal failed: %s", err))
	}

	switch event.EventType {
	case "PerceelGeregistreerd":
		if err := app.HandleRegisteredPlotEvent(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle plot registered event failed: %w", err))
		}
	case "PerceelEigenaarsVastgesteld":
		if err := app.HandlePlotOwnersDetermined(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle plot owner determined failed: %w", err))
		}
	case "PerceelHerverkaveld":
		if err := app.HandleReparcelledEvent(context.Background(), event); err != nil {
			fmt.Print(fmt.Errorf("handle plot split event failed: %w", err))
		}
	default:
		fmt.Printf("event type: %s unknown", event.EventType)
	}
}

func (a *Application) Request(ctx context.Context, method string, path string, data any, v any) error {
	b := new(bytes.Buffer)
	if data != nil {
		if err := json.NewEncoder(b).Encode(data); err != nil {
			return fmt.Errorf("Decoding failed: %w", err)
		}
	}

	req, err := http.NewRequestWithContext(ctx, method, a.baseURL+path, b)
	if err != nil {
		return err
	}

	req.Header.Set("Content-Type", "application/json; charset=utf-8")
	req.Header.Set("Accept", "*/*")
	req.Header.Set("Cache-Control", "no-cache")

	resp, err := a.client.Do(req)
	if err != nil {
		return err
	}

	if resp.StatusCode > 400 {
		body, _ := httputil.DumpResponse(resp, true)
		return fmt.Errorf("response failed with:\n%s", string(body))
	}

	if v != nil {
		if err := json.NewDecoder(resp.Body).Decode(v); err != nil {
			return err
		}
	}

	return nil
}
