package application

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-interpreter/model"
	"gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/fictief-register-voertuigen-interpreter/model/adapter"
)

func (a *Application) HandleRegisteredPlotEvent(ctx context.Context, event *model.Event) error {
	data := &model.PlotRegisteredEvent{}
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if err := a.Request(ctx, http.MethodPost, "/plots", adapter.ToPlot(event, data), nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (a *Application) HandlePlotOwnersDetermined(ctx context.Context, event *model.Event) error {
	data := &model.PlotOwnersDeterminedEvent{}
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if err := a.Request(ctx, http.MethodPost, fmt.Sprintf("/plots/%s/owners", event.SubjectIDs[0]), adapter.ToOwnerDetermined(event, data), nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}

func (a *Application) HandleReparcelledEvent(ctx context.Context, event *model.Event) error {
	data := &model.ReparcelledEvent{}
	if err := json.Unmarshal(event.EventData, data); err != nil {
		return fmt.Errorf("json unmarshal failed: %w", err)
	}

	if err := a.Request(ctx, http.MethodPost, "/plots/split", adapter.ToReparcelled(event, data), nil); err != nil {
		return fmt.Errorf("request failed: %w", err)
	}

	return nil
}
